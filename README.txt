CS4513 - Distributed Systems
Project 1

Thomas Lextrait
tlextrait@wpi.edu

Compilation:
Use make

Warning:
This program requires 3GB of RAM, otherwise you might want to change the size of a buffer in file utils.c. The buffer size is defined as a macro and by default is set to 3GB.

RM
./rm [-r|-f|-v|-t] file ...

Examples: 
./rm -r -v -f Hello.txt myFolder myFolder2 myFile.txt
./rm myFile.txt

Notes:
Everything should work as specified in the project:
	•	in-line options supported: -r -f -v -h
	•	option -v makes it verbose and shows all the files as they are deleted
	•	option -t displays the total time elapsed
	•	it is possible to delete more than one file/folder in one execution, by specifying multiple locations in the command (see example above)
	•	it is possible to give RM absolute paths
	•	RM tries to rename(), if it fails, it tries to make a binary copy, then it copies access times and file permissions, then unlink() the original
	•	RM supports file name conflicts and appends .num to folder and file names when necessary
	•	RM is able to recurse over folders for permanent deletion, either when -f is used or when rename() failed, copy() was invoked and the original folder needs to be deleted

DV
./dv [-v] file …

Examples: 
./dv -v myFolder myFile.txt
./dv myFile.txt

Notes:
	•	in-line options supported: -v -h
	•	option -v makes it verbose
	•	DV supports file name conflicts when restoring and appends .num to files and folders restored to current working directory when necessary
	•	If DV fails to rename() it will make a binary copy of the file or a recursive binary copy of the folder and transfer file permissions and access info, then unlink the file or recursively delete the original folder

DUMP
./dump

Notes:
	•	dumping works, it deletes the trash can folder recursively, then restores it.
