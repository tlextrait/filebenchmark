/** File utils.h
 * @author Thomas Lextrait
 */

/**
* Check that the trash directory exists
* and if it doesn't then create it
*/
void checkTrashDir();

/**
* Return the location of the trash directory
* from the environment variable
*/
char* getTrashDirLoc();

/**
* Checks if given path is a directory or a file.
* @return 0 if directory, 1 if file, -1 if doesn't exist
*/
int isDir(char* path);

/**
* Returns 0 is given directory is empty, -1 otherwise
*/
int isDirEmpty(char* path);

/**
* Checks if file/dir exists
* Returns 0 if it does, -1 if it doesn't
*/
int pathExists(char* path);

/**
* Takes a path and creates the destination path that
* will be used to move the item to the trash
*/
char* getDestination(char* path);

/**
* Takes a path and file name and combines them
*/
char* getPath(char* path, char* file);

/**
* Trims the beginning of an absolute path until only
* the filename remains
*/
char* extractFilename(char* path);

/**
* Finds the maximum version # of a file
* (Provide path without .## at the end)
*/
int findMaxVersion(char* path);

/**
* Creates a destination path from the given path, with the correct
* version number appended to the end
*/
char* makeDestinationVersion(char* path);

/**
* Makes a binary copy of a file
*/
void copyBinary(char* source, char* dest);

/**
* Copies an entire directory and all of its files using binary I/O
*/
void copyBinaryDir(char* source, char* dest, int verbose);

/**
* Deletes a directory and all sub-files/directories permanently
*/
void recurseDeleteDir(char* path, int verbose);

/**
* Transfers permissions and file info from source file
* to dest file
*/
void transferInfo(char* source, char* dest);

/* Copyright (C) 1989, 1990, 1991, 1992 Free Software Foundation, Inc.
     Written by James Clark (jjc@jclark.com)
http://opensource.apple.com/source/groff/groff-10/groff/libgroff/itoa.c
*/
char* itoa(int i);

