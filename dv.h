/**
* @file dv.h
* @author Thomas Lextrait
*/

/**
* Print a help message
*/
void help();

/**
* Print an error message when no file/folder was provided
*/
void noFile();

/**
* Restores an item from trash to current working directory
*/
void dive(char* path);
