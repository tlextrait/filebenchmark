/** File utils.c
 * @author Thomas Lextrait
 */

#include <stdio.h>		// remove()
#include <sys/types.h>
#include <stdlib.h>
#include <dirent.h>		// opendir()
#include <sys/stat.h> 	// mkdir()
#include <sys/types.h> 	// mkdir()
#include <unistd.h>		// getcwd()
#include <string.h>
#include <sys/stat.h>	// stat()
#include <utime.h>		// utime()

#include "utils.h"

#define ENV_TRASH 			"TRASH"
#define PATH_MAX_CHAR		500
#define VERSION_MAX_CHAR 	10
#define MAX_CHUNK_SIZE		3221225472	// 3GB max

/**
* Check that the trash directory exists
* and if it doesn't then create it
*/
void checkTrashDir(){
	char* trashDir = getTrashDirLoc();
	
	// Is there nothing in the env variable?
	if(strlen(trashDir) <= 1){
		printf("The TRASH environment variable has not been set.\n");
		exit(1);
	}
	
	// If trash directory doesn't exist, create it
	if(opendir(trashDir) == NULL){
		if(mkdir(trashDir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)!=0){
			printf("Could not create trash directory. Path to trash directory might be invalid, check the TRASH environment variable.\n");
			exit(1);
		}
	}
}

/**
* Return the location of the trash directory
* from the environment variable
*/
char* getTrashDirLoc(){
	// Make a safe copy of the environment variable
	char* trashLoc = (char*)malloc(sizeof(char)*PATH_MAX_CHAR);
	char* env = getenv(ENV_TRASH);
	if(env != NULL){
		strncpy(trashLoc, env, PATH_MAX_CHAR);
	}
	return trashLoc;
}

/**
* Checks if given path is a directory or a file.
* @return 0 if directory, 1 if file, -1 if doesn't exist
*/
int isDir(char* path)
{
	FILE *fp;
	
	if(opendir(path) == NULL){
		fp = fopen(path, "r");
		if(fp == NULL){
			// Doesn't exist
			return -1;
		}else{
			// It's a file
			fclose(fp);
			return 1;
		}
	}else{
		// It's a directory
		return 0;
	}
}

/**
* Returns 0 is given directory is empty, 1 if not empty, -1 if error
*/
int isDirEmpty(char* path)
{
	DIR *dp;
	struct dirent *d;
	
	if(pathExists(path)==0 && isDir(path)==0){
		dp = opendir(path);
		if(dp != NULL){
			d = readdir(dp);
			while(d){
				// ignore '.', '..', '.DS_Store'
				if(
					strcmp(d->d_name, ".")!=0 &&
					strcmp(d->d_name, "..")!=0 &&
					strcmp(d->d_name, ".DS_Store")!=0
				){
					closedir(dp);
					return 1;
				}
				d = readdir(dp);
			}
			closedir(dp);
			return 0;
		}else{
			printf("Could not open directory '%s'\n", path);
			exit(1);
		}
	}else{
		return -1;
	}
}

/**
* Checks if given path is a directory or a file.
* @return 0 if directory, 1 if file, -1 if doesn't exist
*/
int pathExists(char* path)
{
	switch(isDir(path)){
		case 0:
		case 1:
			return 0;
		case -1:
		default:
			return -1;
	}
}

/**
* Takes a path and creates the destination path that
* will be used to move the item to the trash
*/
char* getDestination(char* path)
{
	// Copy the path safely
	char* realPath = (char*)malloc(sizeof(char)*PATH_MAX_CHAR);
	strncpy(realPath, path, PATH_MAX_CHAR);
	// Extract file name from absolute paths
	realPath = extractFilename(realPath);
	
	// Get trash location
	char* destination = getTrashDirLoc();
	
	// Append slash at the end of the path?
	if(
		destination[strlen(destination)-1] != '/' &&
		realPath[0] != '/'
	){
		destination = strcat(destination, "/");
	}
	
	// Append the file/folder path to the end
	destination = strcat(destination, realPath);
	
	// Does file/folder already exist in trash can?
	if(pathExists(destination) == 0){
		destination = makeDestinationVersion(destination);
	}
	
	return destination;
}

/**
* Takes a path and file name and combines them
*/
char* getPath(char* path, char* file)
{
	char* destination = (char*)malloc(sizeof(char)*PATH_MAX_CHAR);
	strncpy(destination, path, PATH_MAX_CHAR);
	
	// Append slash at the end of the path?
	if(
		destination[strlen(destination)-1] != '/' &&
		file[0] != '/'
	){
		destination = strcat(destination, "/");
	}
	
	// Append the file/folder path to the end
	destination = strcat(destination, file);
	
	return destination;
}

/**
* Trims the beginning of an absolute path until only
* the filename remains
*/
char* extractFilename(char* path)
{
	// Find location of last slash before filename
	int i, slashIndex=-1;
	for(i=strlen(path)-1; i>0 && slashIndex==-1; i--){
		if(path[i]=='/'){slashIndex = i;}
	}
	if(slashIndex >= 0){
		path+=slashIndex+1;
	}
	return path;
}

/**
* Finds the maximum version # of a file
* (Provide path without .## at the end)
*/
int findMaxVersion(char* path)
{
	// Make a safe copy of the path
	char* curPath = (char*)malloc(sizeof(char)*PATH_MAX_CHAR);
	strncpy(curPath, path, PATH_MAX_CHAR);
	
	curPath = strcat(curPath, ".1");
	
	// Find lowest version number available
	int counter = 1;
	while(pathExists(curPath)==0){
		counter++;
		// Copy original path again
		strncpy(curPath, path, PATH_MAX_CHAR);
		// Append version
		curPath = strcat(curPath, ".");
		curPath = strcat(curPath, itoa(counter));
	}
	
	// Cleanup
	free(curPath);
	
	return counter-1;
}

/**
* Creates a destination path from the given path, with the correct
* version number appended to the end
*/
char* makeDestinationVersion(char* path)
{
	// Make a copy of the path string
	char* newPath = (char*)malloc(sizeof(char)*PATH_MAX_CHAR);
	strncpy(newPath, path, PATH_MAX_CHAR);
	
	// Find version
	int version = findMaxVersion(newPath)+1;
	char* versionStr = itoa(version);
	
	// Append version
	newPath = strcat(newPath, ".");
	newPath = strcat(newPath, versionStr);
	
	return newPath;
}

/**
* Makes a binary copy of a file
*/
void copyBinary(char* source, char* dest)
{
	FILE *fs, *fd;
	int filesize;
	size_t read;
	char* buffer = (char*)malloc(sizeof(char)*MAX_CHUNK_SIZE);
	
	// Read binary
	fs = fopen(source, "rb");
	
	fseek(fs, 1L, SEEK_END);
	filesize = ftell(fs);
	fseek(fs, 0L, SEEK_SET);
	read = fread(buffer, sizeof(char), filesize, fs);
	
	fclose(fs);
	
	// Write binary
	fd = fopen(dest, "wb");
	fwrite(buffer, sizeof(char), read, fd);
	fclose(fd);
}

/**
* Copies an entire directory and all of its files using binary I/O
*/
void copyBinaryDir(char* source, char* dest, int verbose)
{
	DIR *dp;
	struct dirent *d;
	
	if(pathExists(source)==0){
		if(isDir(source)==0){
			dp = opendir(source);
			if(dp != NULL){
				// Copy self
				if(verbose==0){printf("Copying '%s'\n", source);}
				mkdir(dest, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
				transferInfo(source, dest);
				
				d = readdir(dp);
				while(d){
					if(
						strcmp(d->d_name, ".")!=0 &&	// ignore .
						strcmp(d->d_name, "..")!=0		// ignore ..
					){
						if(isDir(getPath(source, d->d_name))==0){
							copyBinaryDir(getPath(source, d->d_name), getPath(dest, d->d_name), verbose);
						}else{
							if(verbose==0){printf("Copying '%s'\n", getPath(source, d->d_name));}
							copyBinary(getPath(source, d->d_name), getPath(dest, d->d_name));
							transferInfo(getPath(source, d->d_name), getPath(dest, d->d_name));
						}
					}
					d = readdir(dp);
				}
			}else{
				printf("Could not open directory '%s'\n", source);
				exit(1);
			}
		}else{
			if(verbose==0){printf("Copying '%s'\n", source);}
			copyBinary(source, dest);
			transferInfo(source, dest);
		}
	}
}

/**
* Deletes a directory and all sub-files/directories permanently
*/
void recurseDeleteDir(char* path, int verbose)
{
	DIR *dp;
	struct dirent *d;
	
	if(pathExists(path)==0){
		if(isDir(path)==0 && isDirEmpty(path)!=0){
			dp = opendir(path);
			if(dp != NULL){
				d = readdir(dp);
				while(d){
					if(
						strcmp(d->d_name, ".")!=0 &&	// ignore .
						strcmp(d->d_name, "..")!=0		// ignore ..
					){
						if(isDir(getPath(path, d->d_name))==0){
							recurseDeleteDir(getPath(path, d->d_name), verbose);
						}else{
							if(verbose==0){printf("Deleting '%s'\n", getPath(path, d->d_name));}
							remove(getPath(path, d->d_name));
						}
					}
					d = readdir(dp);
				}
				// Delete self
				if(verbose==0){printf("Deleting '%s'\n", path);}
				remove(path);
			}else{
				printf("Could not open directory '%s'\n", path);
				exit(1);
			}
		}else{
			if(verbose==0){printf("Deleting '%s'\n", path);}
			remove(path);
		}
	}
}

/**
* Transfers permissions and file info from source file
* to dest file
*/
void transferInfo(char* source, char* dest)
{
	struct stat buf; 			// File stats
	struct utimbuf puttime;	// used for setting the time
	
	if(pathExists(source) == 0 && pathExists(dest) == 0){
		// Copy access and modify times, and permissions
		if(stat(source, &buf)==0){
			puttime.actime = buf.st_atimespec.tv_sec;
			puttime.modtime = buf.st_mtimespec.tv_sec;
			utime(dest, &puttime);
			chmod(dest, buf.st_mode);
		}else{
			printf("Couldn't read file stats for '%s'\n", source);
		}
	}
}

/* Copyright (C) 1989, 1990, 1991, 1992 Free Software Foundation, Inc.
     Written by James Clark (jjc@jclark.com)
http://opensource.apple.com/source/groff/groff-10/groff/libgroff/itoa.c
*/
char* itoa(int i)
{
  /* Room for VERSION_MAX_CHAR digits, - and '\0' */
  static char buf[VERSION_MAX_CHAR + 2];
  char *p = buf + VERSION_MAX_CHAR + 1;	/* points to terminating '\0' */
  if (i >= 0) {
    do {
      *--p = '0' + (i % 10);
      i /= 10;
    } while (i != 0);
    return p;
  }else {			/* i < 0 */
    do {
      *--p = '0' - (i % 10);
      i /= 10;
    } while (i != 0);
    *--p = '-';
  }
  return p;
}

