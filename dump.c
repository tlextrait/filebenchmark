/** File dump.c
 * @author Thomas Lextrait
 */

#include <stdio.h>		// remove()
#include <sys/types.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h> 	// mkdir()
#include <sys/types.h> 	// mkdir(), copy()
#include <unistd.h>		// getopt()

#include "dump.h"
#include "utils.h"

/**
* GLOBAL VARIABLES
*/
int verbose = -1;

int main(int argc, char *argv[])
{
	// For getopt()
	int c;
	extern int optind, opterr;
	extern char *optarg;
	
	// Parse command line options
	while((c = getopt (argc, argv, "hv")) != EOF) {
	    switch(c){
			default:
		    case '?': // opt error
		    case 'h': // help
		      	help();
				exit(1);
	    }
  	}
	
	checkTrashDir();
	recurseDeleteDir(getTrashDirLoc(), 0);	// Delete trash directory
	checkTrashDir();						// Restore trash directory
	
	return 0;
}

/**
* Print a help message
*/
void help()
{
	printf("dump - empties the trash can - thomas.lextrait@gmail.com\n");
	printf("usage: dump\n");
	printf("\t-h\tDisplay this help message.\n");
}

/**
* Deletes item at given location permanently.
*/
void permaDelete(char* path)
{
	// Find the location in the trash can
	char* realPath = getPath(getTrashDirLoc(), path);
	
	if(pathExists(realPath)==0){
		if(isDir(realPath) == 0){
			if(isDirEmpty(realPath)==0){
				if(remove(realPath)!=0){
					printf("Couldn't delete '%s' permanently.\n", realPath);
				}
			}else{
				recurseDeleteDir(realPath, verbose);
			}
		}else{
			if(remove(realPath)!=0){
				printf("Couldn't delete '%s' permanently.\n", realPath);
			}
		}
	}else{
		printf("Couldn't find '%s'\n", realPath);
	}
}
