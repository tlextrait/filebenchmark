/**
* @file dump.h
* @author Thomas Lextrait
*/

/**
* Print a help message
*/
void help();

/**
* Deletes item at given location permanently.
*/
void permaDelete(char* path);
