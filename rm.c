/** File rm.c
 * @author Thomas Lextrait
 */

#include <stdio.h>		// remove()
#include <sys/types.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h> 	// mkdir()
#include <sys/types.h> 	// mkdir(), copy()
#include <unistd.h>		// getopt()
#include <sys/time.h>

#include "rm.h"
#include "utils.h"

/**
* GLOBAL VARIABLES
*/
int verbose = -1;
int forceDelete = -1;
int recurseDir = -1;
int timeShow = -1;

int main(int argc, char *argv[])
{
	// For getopt()
	int c;
	extern int optind, opterr;
	extern char *optarg;
	
	// Timing
	struct timeval tv1, tv2;
	struct timezone tz;
	long totalTime=0;
	
	if(argc > 1){
		
		// Parse command line options
		while((c = getopt (argc, argv, "hvrft")) != EOF) {
		    switch(c){
			    case 'r': // recurse directories
					recurseDir = 0;
					break;
			    case 'f': // force remove
					forceDelete = 0;
			      	break;
				case 'v': // verbose
					verbose = 0;
					break;
				case 't': // display time
					timeShow = 0;
					break;
				default:
			    case '?': // opt error
			    case 'h': // help
			      	help();
					exit(1);
		    }
	  	}
	
		// Check the trash directory
		checkTrashDir();
		
		if(optind < argc){
			// For each file specified, delete
			int i;
			for(i=optind; i<argc; i++){
				// Delete the item
				if(verbose==0){
					printf("Deleting '%s'\n", argv[i]);
				}
				gettimeofday(&tv1, &tz);
				if(forceDelete==0){
					permaDelete(argv[i]);
				}else{
					moveToTrash(argv[i]);
				}
				gettimeofday(&tv2, &tz);
				totalTime += (tv2.tv_sec - tv1.tv_sec) * 1000 + (tv2.tv_usec - tv1.tv_usec)/1000;
			}
			
			if(timeShow==0){
				printf("Total time: %li milliseconds\n", totalTime);
			}
			
		}else{
			noFile();
		}
	}else{
		noFile();
	}
	
	return 0;
}

/**
* Print a help message
*/
void help()
{
	printf("rm - file/directory remover - thomas.lextrait@gmail.com\n");
	printf("usage: rm [-r|-f|-v|-t] file ...\n");
	printf("\t-r\tRecurse directories and move their sub-files and directories to trash.\n");
	printf("\t-f\tDelete files and directories permanently.\n");
	printf("\t-v\tBe verbose when deleting files, showing them as they are removed.\n");
	printf("\t-t\tDisplay total time elapsed.\n");
	printf("\t-h\tDisplay this help message.\n");
}

/**
* Print an error message when no file/folder was provided
*/
void noFile()
{
	printf("Please specify a file or directory to move to the trash directory.\n");
	exit(1);
}

/**
* Moves item at given location to the trash folder specified
* by the TRASH environment variable
*/
void moveToTrash(char* path)
{
	if(pathExists(path)==0){
		char* destination = getDestination(path);
		
		if(rename(path, destination)!=0){
			
			// Failed, try copy/unlink
			if(isDir(path) == 0){
				if(recurseDir==0){
					copyBinaryDir(path, destination, verbose);
					recurseDeleteDir(path, verbose);
				}else{
					printf("Directory '%s' is not empty. To delete non-empty directories, please allow directory recursion by using the option -r\n", path);
				}
			}else{
				copyBinary(path, destination);
				transferInfo(path, destination);
				if(unlink(path) != 0){
					printf("Couldn't unlink '%s' after binary copy.\n", path);
				}
			}
		}
		
	}else{
		printf("Couldn't find '%s'\n", path);
	}
}

/**
* Deletes item at given location permanently.
*/
void permaDelete(char* path)
{
	if(pathExists(path)==0){
		if(isDir(path) == 0){
			if(isDirEmpty(path)==0){
				if(remove(path)!=0){
					printf("Couldn't delete '%s' permanently.\n", path);
				}
			}else{
				if(recurseDir==0){
					recurseDeleteDir(path, verbose);
				}else{
					printf("Directory '%s' is not empty. To delete non-empty directories, please allow directory recursion by using the option -r\n", path);
				}
			}
		}else{
			if(remove(path)!=0){
				printf("Couldn't delete '%s' permanently.\n", path);
			}
		}
	}else{
		printf("Couldn't find '%s'\n", path);
	}
}
