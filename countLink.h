/**
* @file countLink.h
* @author Thomas Lextrait
*/

/**
* Create num files in a temp folder
* Returns total creation time
*/
int createFiles(int num);

/**
* Renames a large batch of files
* Returns total time
*/
int renameBatch(int num);
