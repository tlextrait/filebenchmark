#######################################
# Thomas Lextrait , tlextrait@wpi.edu
#######################################
CC=gcc
CFLAGS=-c -Wall
#######################################
all: rm dump dv countLink countBig

rm: rm.o utils.o
	$(CC) rm.o utils.o -o rm

rm.o: rm.c rm.h utils.h
	$(CC) $(CFLAGS) rm.c
	
dump: dump.o utils.o
	$(CC) dump.o utils.o -o dump

dump.o: dump.c dump.h utils.h
	$(CC) $(CFLAGS) dump.c

dv: dv.o utils.o
	$(CC) dv.o utils.o -o dv

dv.o: dv.c dv.h utils.h
	$(CC) $(CFLAGS) dv.c
	
#######################################
# Experiment programs
#######################################
countLink: countLink.o utils.o
	$(CC) countLink.o utils.o -o countLink

countLink.o: countLink.c countLink.h utils.h
	$(CC) $(CFLAGS) countLink.c
	
countBig: countBig.o utils.o
	$(CC) countBig.o utils.o -o countBig

countBig.o: countBig.c countBig.h utils.h
	$(CC) $(CFLAGS) countBig.c

#######################################
# Utility functions
#######################################
utils.o: utils.c utils.h
	$(CC) $(CFLAGS) utils.c

#######################################
# Clean up
#######################################
clean:
	rm -f *.o rm dv dump
	
superclean:
	rm -f *.o rm dv dump *~
