/**
* @file rm.h
* @author Thomas Lextrait
*/

/**
* Print a help message
*/
void help();

/**
* Print an error message when no file/folder was provided
*/
void noFile();

/**
* Moves item at given location to the trash folder specified
* by the TRASH environment variable
*/
void moveToTrash(char* path);

/**
* Deletes item at given location permanently.
*/
void permaDelete(char* path);
