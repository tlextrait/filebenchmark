/**
* @file countLink.h
* @author Thomas Lextrait
*/

/**
* Creates one large file in a temp folder
* Returns total creation time
*/
int createBig();

/**
* Moves one file using binary copy
* Returns total time
*/
int moveBinaryBig();
