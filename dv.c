/** File dv.c
 * @author Thomas Lextrait
 */

#include <stdio.h>		// remove()
#include <sys/types.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h> 	// mkdir()
#include <sys/types.h> 	// mkdir(), copy()
#include <unistd.h>		// getopt()

#include "dv.h"
#include "utils.h"

#define PATH_MAX_CHAR		500

/**
* GLOBAL VARIABLES
*/
int verbose = -1;

int main(int argc, char *argv[])
{
	// For getopt()
	int c;
	extern int optind, opterr;
	extern char *optarg;
	
	if(argc > 1){
		
		// Parse command line options
		while((c = getopt (argc, argv, "hv")) != EOF) {
		    switch(c){
				case 'v': // verbose
					verbose = 0;
					break;
				default:
			    case '?': // opt error
			    case 'h': // help
			      	help();
					exit(1);
		    }
	  	}
	
		// Check the trash directory
		checkTrashDir();
		
		if(optind < argc){
			// For each file specified, delete
			int i;
			for(i=optind; i<argc; i++){
				// Restore the item
				if(verbose==0){
					printf("Restoring '%s'\n", argv[i]);
				}
				dive(argv[i]);
			}
		}else{
			noFile();
		}
	}else{
		noFile();
	}
	
	return 0;
}

/**
* Print a help message
*/
void help()
{
	printf("dv - file/directory dive, restores given file/directory to current working directory - thomas.lextrait@gmail.com\n");
	printf("usage: dv [-v] [file|directory] ...\n");
	printf("\t-v\tBe verbose when deleting files, showing them as they are dumped.\n");
	printf("\t-h\tDisplay this help message.\n");
}

/**
* Print an error message when no file/folder was provided
*/
void noFile()
{
	printf("Please specify a file or directory to restore from trash.\n");
	exit(1);
}

/**
* Restores an item from trash to current working directory
*/
void dive(char* path)
{
	// Find the location in the trash can
	char* realPath = getPath(getTrashDirLoc(), path);
	char* curDir = (char*)malloc(sizeof(char)*PATH_MAX_CHAR);
	char* destination = getPath(getcwd(curDir, PATH_MAX_CHAR), path);
	
	if(pathExists(destination) == 0){
		// Avoid conflicts when restoring, append .num
		destination = makeDestinationVersion(destination);
	}
	
	if(pathExists(realPath) == 0){
		if(rename(realPath, destination)!=0){
			if(isDir(realPath) == 0){
				copyBinaryDir(realPath, destination, verbose);
				recurseDeleteDir(realPath, verbose);
			}else{
				copyBinary(realPath, destination);
				transferInfo(realPath, destination);
				if(unlink(realPath) != 0){
					printf("Couldn't unlink '%s' after binary copy.\n", path);
				}
			}
		}
	}else{
		printf("Couldn't find '%s'\n", realPath);
	}
}
