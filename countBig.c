/** File countLink.c
 * @author Thomas Lextrait
 */

#include <stdio.h>		// remove()
#include <sys/types.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h> 	// mkdir()
#include <sys/types.h> 	// mkdir(), copy()
#include <unistd.h>		// getopt()
#include <string.h>
#include <sys/time.h>

#include "countBig.h"
#include "utils.h"

#define CHUNK_SIZE 1073741824	// 1GB

int main()
{
	long timeCreation, timeRename;
	
	// temp folder
	if(pathExists("temp")!=0){
		if(mkdir("temp", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)!=0){
			printf("Couldn't create 'temp' folder\n");
			exit(1);
		}
	}
	
	// temp2 folder (used for renaming)
	if(pathExists("temp2")!=0){
		if(mkdir("temp2", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)!=0){
			printf("Couldn't create 'temp2' folder\n");
			exit(1);
		}
	}
	
	sync(); // flush cache
	timeCreation = createBig();
	sync();
	timeRename = moveBinaryBig();
	
	// delete temp folder
	recurseDeleteDir("temp", -1);
	recurseDeleteDir("temp2", -1);
	
	//remove("/Volumes/Thallium/big");
	
	// print report
	printf("------------------------------------------------------------------------------\n");
	printf("Chunk size:\t%d bytes\n", CHUNK_SIZE);
	printf("------------------------------------------------------------------------------\n");
	printf("Total file creation time:\t%li milliseconds\n", timeCreation);
	printf("Total file moving time:\t\t%li milliseconds\n", timeRename);
	printf("------------------------------------------------------------------------------\n");
	
	return 0;
}

/**
* Creates one large file in a temp folder
* Returns total creation time
*/
int createBig()
{
	int i;
	FILE* fp;
	srand((unsigned)time(NULL));
	
	struct timeval tv1, tv2;
	struct timezone tz;
	long totalTime=0;
	
	char* buffer = (char*)malloc(sizeof(char)*CHUNK_SIZE);
	
	for(i=0; i<CHUNK_SIZE; i++){
		buffer[i] = rand()%100+100;
	}
	
	gettimeofday(&tv1, &tz);
	fp = fopen("temp/big", "wb");
	fwrite(buffer, 1, CHUNK_SIZE, fp);
	fclose(fp);
	gettimeofday(&tv2, &tz);
	totalTime += (tv2.tv_sec - tv1.tv_sec) * 1000 + (tv2.tv_usec - tv1.tv_usec)/1000;
	
	free(buffer);
	
	return totalTime;
}

/**
* Moves one file using binary copy
* Returns total time
*/
int moveBinaryBig()
{
	struct timeval tv1, tv2;
	struct timezone tz;
	long totalTime=0;
	
	gettimeofday(&tv1, &tz);
	copyBinary("temp/big", "temp2/big");
	remove("temp/big");
	gettimeofday(&tv2, &tz);
	totalTime += (tv2.tv_sec - tv1.tv_sec) * 1000 + (tv2.tv_usec - tv1.tv_usec)/1000;
	
	return totalTime;
}
