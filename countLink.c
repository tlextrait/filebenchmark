/** File countLink.c
 * @author Thomas Lextrait
 */

#include <stdio.h>		// remove()
#include <sys/types.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h> 	// mkdir()
#include <sys/types.h> 	// mkdir(), copy()
#include <unistd.h>		// getopt()
#include <string.h>
#include <sys/time.h>

#include "countLink.h"
#include "utils.h"

#define CHUNK_SIZE 1048576

int main(int argc, char *argv[])
{
	int fileNum;
	long timeCreation, timeRename;
	
	// temp folder
	if(pathExists("temp")!=0){
		if(mkdir("temp", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)!=0){
			printf("Couldn't create 'temp' folder\n");
			exit(1);
		}
	}
	
	// temp2 folder (used for renaming)
	if(pathExists("temp2")!=0){
		if(mkdir("temp2", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)!=0){
			printf("Couldn't create 'temp2' folder\n");
			exit(1);
		}
	}
	
	// how many files should be linked/unlinked?
	if(argc == 1){
		fileNum = 1000;
	}else{
		fileNum = atoi(argv[1]);
		if(fileNum < 1){fileNum = 1000;}
	}
	
	sync(); // flush cache
	timeCreation = createFiles(fileNum);
	sync();
	timeRename = renameBatch(fileNum);
	
	// delete temp folder
	recurseDeleteDir("temp", -1);
	recurseDeleteDir("temp2", -1);
	
	// print report
	printf("------------------------------------------------------------------------------\n");
	printf("Files created:\t%d\n", fileNum);
	printf("Chunk size:\t%d bytes\n", CHUNK_SIZE);
	printf("------------------------------------------------------------------------------\n");
	printf("Total file creation time:\t%li milliseconds\n", timeCreation);
	printf("Total file renaming time:\t%li milliseconds\n", timeRename);
	printf("Average creation time/file:\t%li milliseconds\n", timeCreation/fileNum);
	printf("Average renaming time/file:\t%li milliseconds\n", timeRename/fileNum);
	printf("------------------------------------------------------------------------------\n");
	
	return 0;
}

/**
* Create num files in a temp folder
* Returns total creation time
*/
int createFiles(int num)
{
	int i;
	FILE* fp;
	char* folder = "temp/";
	char* filename = (char*)malloc(sizeof(char)*50);
	
	struct timeval tv1, tv2;
	struct timezone tz;
	long totalTime=0;
	
	// Make a buffer with 1024 blocks (1KB of data)
	char* buffer = (char*)malloc(sizeof(char)*CHUNK_SIZE);
	
	for(i=0; i<CHUNK_SIZE; i++){
		buffer[i] = 97;
	}
	
	for(i=0; i<num; i++){
		filename = getPath(folder, itoa(i));
		gettimeofday(&tv1, &tz);
		fp = fopen(filename, "wb");
		fwrite(buffer, sizeof(char), CHUNK_SIZE, fp);
		fclose(fp);
		gettimeofday(&tv2, &tz);
		totalTime += (tv2.tv_sec - tv1.tv_sec) * 1000 + (tv2.tv_usec - tv1.tv_usec)/1000;
	}
	
	free(buffer);
	free(filename);
	
	return totalTime;
}

/**
* Renames a large batch of files
* Returns total time
*/
int renameBatch(int num)
{
	int i;
	char* folder1 = "temp/";
	char* folder2 = "temp2/";
	
	struct timeval tv1, tv2;
	struct timezone tz;
	long totalTime=0;
	
	for(i=0; i<num; i++){
		gettimeofday(&tv1, &tz);
		rename(getPath(folder1, itoa(i)), getPath(folder2, itoa(i)));
		gettimeofday(&tv2, &tz);
		totalTime += (tv2.tv_sec - tv1.tv_sec) * 1000 + (tv2.tv_usec - tv1.tv_usec)/1000;
	}
	
	return totalTime;
}
